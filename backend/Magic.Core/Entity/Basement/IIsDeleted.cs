﻿using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Entity;


/// <summary>
/// 软删除全局过滤器
/// </summary>
public interface IIsDeleted
{
    
    public bool IsDeleted { get; set; }
}
