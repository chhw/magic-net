﻿using Furion.DependencyInjection;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysVisLogService : ITransient
{
    Task Clear();
    Task<PageList<SysLogVis>> PageList(QueryVisLogPageInput input);
}
