﻿using Furion.DependencyInjection;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Core.Service;

public interface ISysUserRoleService : ITransient
{
    Task DeleteUserRoleListByRoleId(long roleId);
    Task DeleteUserRoleListByUserId(long userId);
    Task<List<long>> GetUserRoleDataScopeIdList(long userId, long orgId);
    Task<List<long>> GetUserRoleIdList(long userId);
    Task GrantRole(GrantUserInput input);
    Task<List<RoleOutput>> GetUserRoleList(List<long> userIds);
}
