﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 回收站服务
/// </summary>
[ApiDescriptionSettings(Name = "Trash", Order = 100, Tag = "回收站服务")]
public class TrashController : IDynamicApiController
{
    private readonly ITrashService _service;
    public TrashController(ITrashService service)
    {
        _service = service;
    }

    /// <summary>
    /// 分页查询回收站
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/Trash/page")]
    public async Task<PageList<QueryDocumentPageOutput>> Page([FromQuery] QueryDocumentPageInput input)
    {
        return await _service.Page(input);
    }

    /// <summary>
    /// 恢复一个
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Trash/recover")]
    public async Task Recover(PrimaryKeyParam input)
    {
        await _service.Recover(input);
    }

    /// <summary>
    /// 恢复多个
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Trash/recovers")]
    public async Task Recovers(BatchDeleteDocumentInput input)
    {
        await _service.Recovers(input);
    }

    /// <summary>
    /// 永久删除
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Trash/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    ///批量删除
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/Trash/deletes")]
    public async Task Deletes(BatchDeleteDocumentInput input)
    {
        await _service.Deletes(input);
    }

    /// <summary>
    /// 清空
    /// </summary>
    /// <returns></returns>
    [HttpPost("/Trash/empty")]
    public async Task Empty()
    {
        await _service.Empty();
    }
}
