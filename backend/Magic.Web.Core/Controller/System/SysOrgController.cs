﻿using Furion.DynamicApiController;
using Magic.Core;
using Magic.Core.Entity;
using Magic.Core.Service;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 组织机构服务
/// </summary>
[ApiDescriptionSettings(Name = "Org", Order = 100, Tag = "组织机构服务")]
public class SysOrgController : IDynamicApiController
{
    private readonly ISysOrgService _service;
    public SysOrgController(ISysOrgService service)
    {
        _service = service;
    }
    /// <summary>
    /// 分页查询组织机构
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysOrg/page")]
    public async Task<dynamic> PageList([FromQuery] QueryOrgPageInput input)
    {
        return await _service.PageList(input);
    }

    /// <summary>
    /// 获取组织机构列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysOrg/list")]
    public async Task<List<OrgOutput>> List([FromQuery] QueryOrgPageInput input)
    {
        return await _service.List(input);
    }

    /// <summary>
    /// 增加组织机构
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysOrg/add")]
    public async Task AddOrg(AddOrgInput input)
    {
        await _service.Add(input);
    }

    /// <summary>
    /// 删除组织机构
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysOrg/delete")]
    public async Task Delete(PrimaryKeyParam input)
    {
        await _service.Delete(input);
    }

    /// <summary>
    /// 更新组织机构
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost("/sysOrg/edit")]
    public async Task Update(EditOrgInput input)
    {
        await _service.Update(input);
    }

    /// <summary>
    /// 获取组织机构信息
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysOrg/detail")]
    public async Task<SysOrg> Get([FromQuery] PrimaryKeyParam input)
    {
        return await _service.Get(input);
    }

    /// <summary>
    /// 获取组织机构树       
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysOrg/tree")]
    public async Task<List<OrgTreeNode>> GetOrgTree()
    {
        return await _service.GetOrgTree();
    }
}
