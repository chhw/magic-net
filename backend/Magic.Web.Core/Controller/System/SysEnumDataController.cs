﻿using Furion.DynamicApiController;
using Magic.Core.Service;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Magic.Web.Core;

/// <summary>
/// 枚举值服务
/// </summary>
[ApiDescriptionSettings(Name = "EnumData", Order = 100, Tag = "枚举值服务")]
[AllowAnonymous]
public class SysEnumDataController : IDynamicApiController
{
    private readonly ISysEnumDataService _service;

    public SysEnumDataController(ISysEnumDataService service)
    {
        _service = service;
    }


    /// <summary>
    /// 获取所有枚举值
    /// </summary>
    /// <returns></returns>
    [HttpGet("/sysEnumData/enumTypeList")]
    public dynamic GetEnumTypeList()
    {
        return _service.GetEnumTypeList();
    }
    /// <summary>
    /// 通过枚举类型获取枚举值集合
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysEnumData/list")]
    public async Task<IEnumerable<EnumDataOutput>> GetEnumDataList([FromQuery] EnumDataInput input)
    {
        return await _service.GetEnumDataList(input);
    }

    /// <summary>
    /// 通过实体字段类型获取相关集合（目前仅支持枚举类型）
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet("/sysEnumData/listByFiled")]
    public async Task<IEnumerable<EnumDataOutput>> GetEnumDataListByField([FromQuery] QueryEnumDataInput input)
    {
        return await _service.GetEnumDataListByField(input);
    }
}
